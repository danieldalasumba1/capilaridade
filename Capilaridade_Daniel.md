# Capilaridade

O objetivo principal deste projeto é apresentar temas relacionados à Capilaridade.

## Introdução

Denomina-se capilaridade a capacidade que as substâncias possuem de subirem e descerem em tubos capilares (tubos finos), ou seja a capacidade que elas possuem de se locomover em curto espaço de material poroso, como por exemplo em uma esponja, pano ou algodão, eles se locomovem até mesmo contra a força de gravidade.

Como explicar a formação de gotículas de água? Por que os cabelos molhados ficam juntos? Por que o sabonete ajuda na limpeza? Como os insetos e as agulhas podem permanecer na água?

Entender melhor essas questões é uma de nossas principais motivações.

<img src = "Img/Fig_5.jpeg" width = "600">



É interessante mencionar a distinção entre uma agulha e um barco na água.

A densidade tem um papel fundamental nesta questão. Em uma porção de água, considere qualquer superfície fechada imaginária.

Se essa porção de água permanece em repouso, é porque a força resultante sobre ela é zero. Somente a força da gravidade e as interações com a água fora da fronteira agem sobre ela.

$`\vec{P_w} + \vec{E} = \vec{0} \implies \vec{E} = -m_w \vec{g}`$

Mas essas forças de contato, que dão origem à flutuabilidade, não serão diferentes se o material dentro dessa fronteira for diferente.

$`\sum{\vec{F}} = \vec{P} + \vec{E} = (m - m_w)\vec{g} = (\rho - \rho_w) V \vec{g}`$

O volume é o mesmo em ambos os casos.



Neste modelo, a densidade determina se o objeto flutua ou afunda.

<img src = "Img/Fig_9.jpeg" width = "250">

O barco flutua porque sua densidade é menor do que a densidade da água. Ele permanece na água devido à força de flutuação.

A agulha, por outro lado, é totalmente preenchida com metal, sendo mais densa que a água. E com um pequeno empurrão ele afundará como esperado desde o início. Portanto, sua permanência na superfície não pode ser explicada pela flutuabilidade, e a força que sustenta seu peso resulta de um fenômeno diferente.

A coesão mantém as moléculas dos líquidos ligados entre si, através da atração intermolecular,já a adesão está relacionada a atração das moléculas da água com a molécula do tubo sólido. O fenômeno da capilaridade ocorre quando as moléculas da água colidem e aderem com as do tubo através da adesão, e afastam as demais através da coesão.

Quanto maior o diâmetro do tubo, menor é o número de moléculas de líquido que se aderem a parede em relação às que são arrastadas para cima por coesão. Sendo assim, sabendo o diâmetro do tubo pode se calcular a altura que o líquido atinge em seu interior, através da força da capilaridade.

Quanto mais fino for o tubo maior a aderência e quanto mais quente for o líquido menos viscoso será o líquido, ou seja, o fator viscosidade depende da temperatura.

Imaginemos a seguinte situação: Um recipiente com água e pegamos um outro objeto como por exemplo palha, se eu mergulhar uma parte dele na água, então como as moléculas de água são atraídas pela parede desse recipiente menor que estará dentro dele na água, como as moléculas de água são atraídas pela parede do recipiente menor que estará dentro,a água não permanecerá no mesmo nível,ou seja o nível de água vai subir até um certo nível maior do que a do recipiente maior,e se o recipiente for mais fino maior vai ser o nível de água que vai subir pelo recipiente. Esse fenômeno é o que chamamos de capilaridade.

## Origem 



A água é a principal substância na qual o efeito capilar é geralmente observado.



As partes que constituem o líquido formam conexões entre si. No entanto, as partículas na interface podem fazer apenas metade do número de conexões.

<img src = "Img/Fig_8.jpeg" width = "428"><img src = "Img/Fig_10.jpeg" width ="428">  

Assim, a <b> Energia de Coesão </b> de uma partícula no meio do líquido, é dada por:

$`U\approx n U_l`$

-  $`n`$ é o número de limites.

-  $`U_l`$ é a <b> Energia de Ligação </b>.

- $`U_l = k_B T`$

-  $`k_B = 1,38064852 × 10 ^ {- 23} \ frac {J} {K}`$ é a constante de Boltzmann.

- $`U_l \approx \frac{1}{40} eV`$ para $`T = 300K`$

Para uma molécula na interface:

$`\Delta U \simeq \frac{n}{2} U_l`$

A <b> Energia de superfície </b> será proporcional à área de superfície.

$`\boxed{U_\Sigma = \gamma \Sigma}`$

$`\ gamma`$ é a <b> tensão superficial </b>. No Sistema Internacional $`[\ gamma] = \ frac {J} {m ^ 2}`$.

Podemos encontrar a relação entre a energia de superfície e a energia de ligação.

$`U_\Sigma = \sum_{i=1}^N [\frac{n}{2} U_l] = \frac{n}{2} U_l \sum_{i=1}^N 1 = \frac{n}{2} U_l N`$

$`N`$ é o número total de partículas de superfície. Avaliando a área da molécula por $`a ^ 2`$, temos:

$`N \simeq \frac{\Sigma}{a^2}`$

Podemos imaginar que cada molécula ocupa um quadrado de lado $`a`$.



A nova relação para $`U_\Sigma`$ é:

$`\boxed{U_\Sigma = \frac{\frac{n}{2} U_l}{a^2}\Sigma}`$ 

Aqui podemos ver a relação entre a tensão superficial $`\gamma`$ e as características das moléculas.

$`\gamma\simeq\frac{\frac{n}{2} U_l}{a^2}`$

## Energia e Trabalho 



Para aumentar a área de superfície, deve-se trabalhar para romper as conexões no meio do fluido. Podemos encontrar a energia necessária para aumentar a área em $`d\Sigma`$.

$`U_\Sigma = \gamma\Sigma\implies\boxed{dU_\Sigma =\gamma d\Sigma}`$

Tomando um caso simples, como o da imagem abaixo, onde a área variada tem uma forma retangular e um lado desse retângulo tem um comprimento fixo $`l`$.

$`d\Sigma = l dx`$

Neste caso, a <b> Força Capilar </b> será:

$`\boxed{\vec\gamma = - \frac{dU_{\Sigma}}{dx} \hat{x} = -\gamma l\hat{x}}`$

<img src = "Img/Fig_6.jpeg" width = "428 ">

### Teorema de Laplace 



A diferença de pressão entre os fluidos é dada por:



$`\boxed{\Delta p = \gamma (\frac{1}{R} + \frac{1}{R'}) = \gamma C}`$

Onde $`C = \ frac {1} {R} + \ frac {1} {R '}`$ é o <b> Raio da Curvatura </b> da superfície que faz fronteira com os fluidos.



Para mais informações sobre este assunto, leia a seção de recomendações.



Como um caso particular, se a superfície for esférica $`R = R`$. Então $`C = \frac{2}{R}`$ e finalmente:

$`\Delta p =\frac{2\gamma}{R}`$ 

<img src = "Img/Drop_Area_Variation.jpeg" width = "550">

## Recomendações 



- O canal [Faculdade de Khan] (https://www.youtube.com/channel/UCGDanWUzNMbIV11lcNi-yBg) tem uma lista de reprodução sobre Geometria Diferencial. Para o nosso assunto, o vídeo em [Curvature] (https://www.youtube.com/watch?v=URr6ZXhZMJY&list=PLdgVBOaXkb9DJjk8V0-RkXTnD4ZXUOFsc&index=5) pode ser de grande ajuda.

-  Existem vídeos introdutórios úteis de [Victor Ugaz] (https://www.youtube.com/channel/UC2OlDnoOOBYj5BkEsl5H9Dg). A [parte 1] (https://www.youtube.com/watch?v=YdLeLvNfE-w) é uma introdução e a [parte 2] (https://www.youtube.com/watch?v=PezcU4hHML4) é sobre a equação de Young-Laplace.

-  I vídeo útil sobre [Contact Angle] (https://www.youtube.com/watch?v=9WigcUFS3eQ).



 - Para estudar <b> Superfícies mínimas </b> mais profundamente, precisamos usar <b> Cálculo de variações </b>. Este [vídeo] (https://www.youtube.com/watch?v=V0wx0JBEgZc) é uma boa introdução e esta [lista de reprodução] (https://www.youtube.com/watch?v=6HeQc7CSkZs&list=PLdgVBOaXkb9CD8igcUr9Fmn5WXLpE8ZE_) é muito útil para obter muitas idéias importantes.








